﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Escriba una pelicula: ");

            WebRequest req = WebRequest.Create("http://www.omdbapi.com/?t=" + Console.ReadLine());

            WebResponse respuesta;

            try
            {
                respuesta = req.GetResponse();
            }

            catch (WebException)
            {
                Console.WriteLine("Error, no hay conexión");
                Console.ReadLine();
                Environment.Exit(0);
            }

            respuesta = req.GetResponse();

            Stream stream = respuesta.GetResponseStream();

            StreamReader sr = new StreamReader(stream);

            JObject data = JObject.Parse(sr.ReadToEnd());

            string error = (string)data["Error"];

            if (error == "Movie not found!")
            {
                Console.WriteLine("La pelicula no existe :(");
            }
            else
            {
                string titulo = (string)data["Title"];
                string Year = (string)data["Year"];
                string Director = (string)data["Director"];
                string MetaScore = (string)data["Metascore"];

                Console.WriteLine("Titulo: " + titulo);
                Console.WriteLine("Año: " + Year);
                Console.WriteLine("Director: " + Director);
                Console.WriteLine("Metascore: " + MetaScore);
            }
            Console.ReadLine();
        }

    }
}
