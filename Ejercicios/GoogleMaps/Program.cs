﻿using System;
using System.Net;
using System.IO;

using Newtonsoft.Json.Linq;

namespace GoogleMaps
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Escriba una ciudad: ");

            WebRequest req = WebRequest.Create("https://maps.googleapis.com/maps/api/geocode/json?address=" + Console.ReadLine());

            WebResponse respuesta;

            try
            {
                respuesta = req.GetResponse();
            }

            catch(WebException)
            {
                Console.WriteLine("Error, no hay conexión");
                Console.ReadLine();
                Environment.Exit(0);
            }

            respuesta = req.GetResponse();

            Stream stream = respuesta.GetResponseStream();

            StreamReader sr = new StreamReader(stream);

            JObject data = JObject.Parse(sr.ReadToEnd());

            string error = (string)data["status"];

            if (error == "ZERO_RESULTS")
            {
                Console.WriteLine("No existe esta ciudad...");
            }
            else
            {
                string pais = (string)data["results"][0]["address_components"][2]["long_name"];

                Console.WriteLine("Pais: " + pais);
            }
            Console.ReadLine();
        }
    }
}
